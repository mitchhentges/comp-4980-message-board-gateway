# Messageboard Controller

## HTTP API

Errors are always returned in the form:

```
{
  "_type": "ERROR",
  "data": ...
}
```

### Client to Server

#### `POST /messages`

Will add another message to list. The message should be specified in the body of the request.
If the list is already full (3 messages), the oldest message is replaced by the new
message. Will trigger a, [`UPDATE_MESSAGE_LIST` websocket request](#update_message_list). Returns the message added,
with its generated UUID.

```
{
  "message": "I'm a message",
  "uuid": "bfd0b9cd-21a0-4c73-9745-3485f348edb5"
}
```

#### `GET /messages`

Returns the list of messages.

```
[
  {
    "message": "I'm a message",
    "uuid": "bfd0b9cd-21a0-4c73-9745-3485f348edb5"
  },
  {
    "message": "I'm another message",
    "uuid": "26fe044d-b91e-458d-af1b-e71f3e22ecfd"
  }
]
```

#### `DELETE /messages/:uuid`

Deletes the message with the specified UUID, or returns a 404 if it couldn't find it. If the message was deleted,
triggers an [`UPDATE_MESSAGE_LIST` websocket request](#update_message_list) and returns the removed message.

```
{
  "message": "I'm a message",
  "uuid": "bfd0b9cd-21a0-4c73-9745-3485f348edb5"
}
```

#### `POST /submitAlert`

Submits a high-priority alert to the connected message boards with the
[`ALERT` websocket request](#alert). The message should be specified in the body of the request.
Returns the submitted message string after encoding as JSON.

```
"Tell my wife I said \"Hello\""
```

#### `GET /eventTypes`

Receives a list of all event types that have been [submitted by clients](#event-submission))

```
[
  "PING",
  "WIND_SPEED",
  "BRIGHTNESS"
]
```

#### `GET /events/:eventType`

Receives all events of the specified type, grouped by device and sorted by date.
[Events are submitted by clients over websocket](#event-submission)

```
{
  "home": [
    {
      "date": "2016-12-05T02:04:45+00:00",
      "value": -432.581,
      "context": {
        "_type": "WIND_SPEED",
        "date": "2016-12-05T02:04:45Z",
        "value": -432.581
      }
    },
    {
      "date": "2016-12-05T02:04:46+00:00",
      "value": 543,
      "context": {
        "_type": "WIND_SPEED",
        "date": "2016-12-05T02:04:46Z",
        "value": 543
      }
    },
    {
      "date": "2016-12-06T02:04:46+00:00",
      "value": 6,
      "context": {
        "_type": "WIND_SPEED",
        "date": "2016-12-06T02:04:46Z",
        "value": 6
      }
    }
  ],
  "other": [
    {
      "date": "2016-12-06T01:34:53.871+00:00",
      "value": 123,
      "context": {
        "_type": "WIND_SPEED",
        "date": "2016-12-06T01:34:53.871Z",
        "device": "other",
        "missesHarambe": true,
        "networkConnection": "1Gbps",
        "value": 123
      }
    }
  ]
}
```

## Websocket API

### Server to Client

Every piece of data sent from the server to the client has the form:

```
{
  "_type": ...,
  "data": ...
}
```

#### `UPDATE_MESSAGE_LIST`

Sent from the server to connected clients whenever the message list is modified.

```
{
  "_type": "UPDATE_MESSAGE_LIST",
  "data": [
      "I'm a message",
      "I'm also a message"
  ]
}
```

#### `ALERT`

Sent from the server to connected clients whenever an alert is submitted

```
{
  "_type": "ALERT",
  "data": "This is important"
}
```

### Client to Server

#### Event submission

Every piece of data sent from the client to the server is an event. It must be json, and it must have at least a
`_type`, `value`, `date` and `device` field. Events can have other properties as well, the server will persist them.

```
{
  "_type": "PING",
  "date": "2016-12-06T00:16: 55.365Z",
  "value": 123,
  "device": "home",
  "networkConnection": "1Gbps",
  "missesHarambe": true
}
```

## Development

1. Install [`rustup`](https://rustup.rs/). Choose to use `nightly` instead of `stable`
2. Install [postgreSQL](https://www.postgresql.org/). Create a new user/database pair
3. Create `config.ini` with `DB_NAME` (database name), `DB_USER` (database username), `DB_PASS` (database password)
4. `cargo run`