#![feature(proc_macro)]

#[macro_use] extern crate serde_derive;
#[macro_use] extern crate itertools;

extern crate chrono;
extern crate hyper;
extern crate ini;
extern crate postgres;
extern crate serde;
extern crate serde_json;
extern crate uuid;
extern crate ws;

use hyper::method::Method;
use hyper::server::{Server, Request, Response};
use hyper::status::StatusCode;
use ini::Ini;
use postgres::{Connection, TlsMode};
use std::io::Write;
use std::thread;
use std::sync::{Arc, Mutex};
use uuid::Uuid;

mod admin;
mod board;

fn main() {
    let config = Ini::load_from_file("config.ini").expect("config.ini file is required");
    let db_user = config.get_from_or(None::<String>, "DB_USER", "messageboard");
    let db_name = config.get_from_or(None::<String>, "DB_NAME", "messageboard");
    let db_host = config.get_from_or(None::<String>, "DB_HOST", "localhost");
    let db_pass_result = config.get_from(None::<String>, "DB_PASS");

    let conn = if let Some(db_pass) = db_pass_result {
        Connection::connect(format!("postgres://{}:{}@{}/{}", db_user, db_pass, db_host, db_name),
                            TlsMode::None)
    } else {
        Connection::connect(format!("postgres://{}@{}/{}", db_user, db_host, db_name), TlsMode::None)
    }.unwrap();

    let shared_conn = Arc::new(Mutex::new(conn));
    let senders = Arc::new(Mutex::new(Vec::<board::WsConnection>::new()));
    let message_list = Arc::new(Mutex::new(Vec::<admin::AdminMessage>::new()));
    {
        let shared_conn = shared_conn.clone();
        let senders = senders.clone();
        let message_list = message_list.clone();
        thread::spawn(move || {
            // When websocket connection dies, gracefully restart the thread
            loop {
                let shared_conn = shared_conn.clone();
                let senders = senders.clone();
                let message_list = message_list.clone();
                let thread = thread::spawn(move || {
                    println!("WS Server started on localhost:9011");
                    if let Err(error) = ws::listen("0.0.0.0:9011", |out| {
                        board::BoardHandler {
                            out: out,
                            uuid: Uuid::new_v4(),
                            address: None,
                            all_senders: senders.clone(),
                            message_list: message_list.clone(),
                            db_conn: shared_conn.clone()
                        }
                    }) {
                        println!("Failed to create WS Server due to {:?}", error);
                    }
                });
                if thread.join().is_err() {
                    println!("Failed to join WS thread after WS error");
                }
            }
        });
    }

    let admin_handler = admin::AdminHandler {
        all_senders: senders,
        message_list: message_list,
        db_conn: shared_conn
    };

    println!("HTTP Server running at 0.0.0.0:13443");
    Server::http("0.0.0.0:13443").unwrap()
        .handle(move |req: Request, mut res: Response| {
            {
                let headers = res.headers_mut();
                headers.set_raw("content-type", vec!(b"application/json; charset=utf-8".to_vec()));
                headers.set_raw("access-control-allow-origin", vec!(b"*".to_vec()));
                headers.set_raw("access-control-allow-methods", vec!(b"GET, POST, DELETE".to_vec()));
            }

            if req.method == Method::Options {
                return;
            }

            let uri_string = format!("{}", req.uri);
            if let Some(endpoint) = uri_string.split("/").nth(1) {
                if endpoint == "messages" && req.method == Method::Post {
                    admin_handler.push_message(req, res);
                    return;
                } else if endpoint == "messages" && req.method == Method::Get {
                    admin_handler.list_messages(req, res);
                    return;
                } else if endpoint == "messages" && req.method == Method::Delete {
                    admin_handler.delete_message(req, res);
                    return;
                } else if endpoint == "submitAlert" && req.method == Method::Post {
                    admin_handler.submit_alert(req, res);
                    return;
                } else if endpoint == "events" && req.method == Method::Get {
                    admin_handler.list_events(req, res);
                    return;
                } else if endpoint == "eventTypes" && req.method == Method::Get {
                    admin_handler.list_event_types(req, res);
                    return;
                }
            }


            let endpoint_list = ["POST /messages", "GET /messages", "DELETE /messages/:uuid",
                "POST /submitAlert", "GET /eventTypes", "GET /events/:eventType"]
                .iter()
                .fold("".to_owned(), |acc, &endpoint| format!("{}{}\n", acc, endpoint));
            let message = format!("Valid endpoints:\n{}", endpoint_list);
            let err = admin::Container::new_err(&message);

            *res.status_mut() = StatusCode::NotFound;
            res.start().unwrap().write_all(serde_json::to_string(&err).unwrap().as_bytes()).unwrap();
        }).unwrap();
}
