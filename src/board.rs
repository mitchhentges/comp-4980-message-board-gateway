use admin;
use chrono::{DateTime, UTC};
use postgres::Connection;
use serde_json;
use serde_json::value::{Map, Value};
use std::ops::Deref;
use std::sync::{Arc, Mutex};
use uuid::Uuid;
use ws;
use ws::Handler;
use ws::Sender;

const PING: ws::util::Token = ws::util::Token(2);
const PING_TIME: u64 = 60_000;

pub struct WsConnection {
    pub sender: Sender,
    pub uuid: Uuid,
}

pub struct BoardHandler {
    pub out: Sender,
    pub uuid: Uuid,
    pub address: Option<String>,
    pub all_senders: Arc<Mutex<Vec<WsConnection>>>,
    pub message_list: Arc<Mutex<Vec<admin::AdminMessage>>>,
    pub db_conn: Arc<Mutex<Connection>>
}

impl Handler for BoardHandler {
    fn on_open(&mut self, handshake: ws::Handshake) -> ws::Result<()> {
        self.out.timeout(PING_TIME, PING)?;

        let client_address = handshake.remote_addr().unwrap().unwrap();
        println!("{}: [connect]", client_address);
        self.address = Some(client_address);

        let message_list = self.message_list.lock().unwrap();

        let container = admin::Container {
            _type: "UPDATE_MESSAGE_LIST".to_owned(),
            data: &message_list.deref().into_iter()
                .map(|admin_message| admin_message.message.clone())
                .collect::<Vec<String>>()
        };

        self.out.send(serde_json::to_string(&container).unwrap()).unwrap();

        self.all_senders.lock().unwrap().push(WsConnection {
            sender: self.out.clone(),
            uuid: self.uuid.clone()
        });
        Ok(())
    }

    fn on_message(&mut self, msg: ws::Message) -> ws::Result<()> {
        if let Ok(text_msg) = msg.clone().as_text() {
            if let Ok(map) = serde_json::from_str::<Map<String, Value>>(&text_msg) {
                if let (Some(&Value::String(ref _type)),
                    Some(&Value::String(ref device)),
                    Some(date),
                    Some(value_container))
                = (map.get("_type"), map.get("device"), map.get("date")
                    .and_then(|value| value.as_str())
                    .and_then(|str| str.parse::<DateTime<UTC>>().ok()), map.get("value")) {
                    if let Some(value) = match value_container {
                        &Value::F64(value) => Some(value),
                        &Value::U64(value) => Some(value as f64),
                        &Value::I64(value) => Some(value as f64),
                        _ => None
                    } {
                        println!("{}: {:#?}", self.address.as_ref().unwrap(), map);
                        let map_value = &serde_json::from_str::<Value>(text_msg).unwrap();
                        let db_conn = self.db_conn.lock().unwrap();
                        db_conn.execute("INSERT INTO events (type, date, value, device, context) VALUES ($1, $2, $3, $4, $5)",
                                        &[&_type, &date, &value, &device, &map_value]).unwrap();
                    } else {
                        println!("! {}: \"value\" must be a number: \"{}\"", self.address.as_ref().unwrap(), text_msg);
                    }
                } else {
                    println!("! {}: Missing \"_type\", \"device\", \"date\" or \"value\": \"{}\"",
                             self.address.as_ref().unwrap(), text_msg);
                }
            } else {
                println!("! {}: Couldn't parse JSON: \"{}\"", self.address.as_ref().unwrap(), text_msg);
            }
        } else {
            println!("{}: [no message]", self.address.as_ref().unwrap());
        }
        Ok(())
    }

    fn on_close(&mut self, _: ws::CloseCode, _: &str) {
        println!("{}: [disconnect]", self.address.as_ref().unwrap());

        let mut senders = self.all_senders.lock().unwrap();
        let index = senders.iter().position(|conn| conn.uuid == self.uuid).unwrap();
        senders.remove(index);
    }

    fn on_timeout(&mut self, tok: ws::util::Token) -> ws::Result<()> {
        match tok {
            PING => {
                self.out.ping(Vec::new())?;
                self.out.timeout(PING_TIME, PING)
            }
            _ => unreachable!()
        }
    }
}