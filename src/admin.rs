use board::WsConnection;
use chrono::{DateTime, UTC};
use hyper::server::{Request, Response};
use hyper::status::StatusCode;
use itertools::Itertools;
use postgres::Connection;
use serde::{Serialize, Serializer};
use serde_json;
use serde_json::value::{Value};
use std::io::{Read, Write};
use std::ops::Deref;
use std::str::FromStr;
use std::sync::{Arc, Mutex};
use uuid::Uuid;

const MAX_MESSAGES: usize = 3;

pub struct AdminHandler {
    pub all_senders: Arc<Mutex<Vec<WsConnection>>>,
    pub message_list: Arc<Mutex<Vec<AdminMessage>>>,
    pub db_conn: Arc<Mutex<Connection>>
}

impl AdminHandler {
    pub fn list_event_types(&self, _: Request, res: Response) {
        let db_conn = self.db_conn.lock().unwrap();
        let event_types: Vec<String> = if let Ok(rows) = db_conn.query("SELECT DISTINCT type FROM events", &[]) {
            rows.iter().map(|row| row.get(0)).collect()
        } else {
            vec!()
        };
        res.start().unwrap().write_all(serde_json::to_string(&event_types).unwrap().as_bytes()).unwrap();
    }

    pub fn list_events(&self, req: Request, mut res: Response) {
        let uri_string = format!("{}", req.uri);
        let db_conn = self.db_conn.lock().unwrap();
        if let Some(_type) = uri_string.split("/").nth(2) {
            let mut raw_events: Vec<(String, String, f64, Value)> = if let Ok(rows)
            = db_conn.query("SELECT device, date, value, context FROM events WHERE type = $1", &[&_type]) {
                rows.iter().map(|row| {
                    let device: String = row.get(0);
                    let date: String = row.get::<usize, DateTime<UTC>>(1).to_rfc3339();
                    let value: f64 = row.get(2);
                    let context: Value = row.get(3);
                    (device, date, value, context)
                })
                    .collect()
            } else {
                vec!()
            };
            raw_events.sort_by_key(|event| event.0.clone()); // Group_by is lazy, need to sort ahead of time
            let admin_events: Vec<(String, Vec<AdminEvent<Value>>)> = raw_events.into_iter()
                .group_by(|raw| raw.0.clone())
                .into_iter()
                .map(|(key, value)| {
                    let mut device_event_list = value.into_iter()
                        .map(|it| AdminEvent {
                            date: it.1,
                            value: it.2,
                            context: it.3
                        })
                        .collect::<Vec<AdminEvent<Value>>>();
                    device_event_list.sort_by_key(|event| event.date.clone());
                    (key, device_event_list)
                })
                .collect();
            let response = AdminEventResponse {
                pairs: admin_events
            };
            res.start().unwrap().write_all(serde_json::to_string(&response).unwrap().as_bytes()).unwrap();
        } else {
            let message = "Missing event type to query for in uri";
            let err = Container::new_err(&message);
            *res.status_mut() = StatusCode::BadRequest;
            res.start().unwrap().write_all(serde_json::to_string(&err).unwrap().as_bytes()).unwrap();
        }
    }

    pub fn push_message(&self, mut req: Request, mut res: Response) {
        let mut message = String::new();
        req.read_to_string(&mut message).unwrap();

        if message.is_empty() {
            let message = "Message contents were not specified";
            let err = Container::new_err(&message);

            *res.status_mut() = StatusCode::BadRequest;
            res.start().unwrap().write_all(serde_json::to_string(&err).unwrap().as_bytes()).unwrap();
            return;
        }

        let mut message_list = self.message_list.lock().unwrap();
        if message_list.len() == MAX_MESSAGES {
            message_list.remove(0);
        }

        let new_message = AdminMessage {
            message: message,
            uuid: Uuid::new_v4()
        };
        res.send(serde_json::to_string(&new_message).unwrap().as_bytes()).unwrap();

        message_list.push(new_message);
        let container = Container {
            _type: "UPDATE_MESSAGE_LIST".to_owned(),
            data: &message_list.deref().into_iter()
                .map(|admin_message| admin_message.message.clone())
                .collect::<Vec<String>>()
        };

        let senders = self.all_senders.lock().unwrap();
        for ws_connection in senders.deref() {
            ws_connection.sender.send(serde_json::to_string(&container).unwrap()).unwrap();
        }
    }

    pub fn list_messages(&self, _: Request, res: Response) {
        res.send(serde_json::to_string(&self.message_list.lock().unwrap().deref()).unwrap().as_bytes()).unwrap();
    }

    pub fn delete_message(&self, req: Request, mut res: Response) {
        let uri_string = format!("{}", req.uri);
        if let Some(uuid) = uri_string.split("/").nth(2).and_then(|uuid| Uuid::from_str(uuid).ok()) {
            let mut message_list = self.message_list.lock().unwrap();

            if let Some(index) = message_list.iter().position(|message| message.uuid == uuid) {
                res.send(serde_json::to_string(&message_list.remove(index)).unwrap().as_bytes()).unwrap();

                let container = Container {
                    _type: "UPDATE_MESSAGE_LIST".to_owned(),
                    data: &message_list.deref().into_iter()
                        .map(|admin_message| admin_message.message.clone())
                        .collect::<Vec<String>>()
                };

                let senders = self.all_senders.lock().unwrap();
                for ws_connection in senders.deref() {
                    ws_connection.sender.send(serde_json::to_string(&container).unwrap()).unwrap();
                }
            } else {
                let message = format!("Could not find message with uuid \"{}\"", uuid);
                let err = Container::new_err(&message);

                *res.status_mut() = StatusCode::NotFound;
                res.start().unwrap().write_all(serde_json::to_string(&err).unwrap().as_bytes()).unwrap();
            }
        } else {
            let message = "Invalid uuid in uri";
            let err = Container::new_err(&message);
            *res.status_mut() = StatusCode::BadRequest;
            res.start().unwrap().write_all(serde_json::to_string(&err).unwrap().as_bytes()).unwrap();
        }
    }

    pub fn submit_alert(&self, mut req: Request, mut res: Response) {
        let mut message = String::new();
        req.read_to_string(&mut message).unwrap();

        if message.is_empty() {
            *res.status_mut() = StatusCode::BadRequest;
            res.start().unwrap().write_all(b"Body of request was empty").unwrap();
            return;
        }

        res.send(serde_json::to_string(&message).unwrap().as_bytes()).unwrap();

        let container = Container {
            _type: "ALERT".to_owned(),
            data: &message
        };

        let senders = self.all_senders.lock().unwrap();
        for ws_connection in senders.deref() {
            ws_connection.sender.send(serde_json::to_string(&container).unwrap()).unwrap();
        }
    }
}

/// Representation of a message only shared between the admin interface and this control server.
/// Each message is given a unique ID for referencing
#[derive(Serialize)]
pub struct AdminMessage {
    pub message: String,
    uuid: Uuid
}

pub struct AdminEventResponse<T>
    where T: Serialize {
    pairs: Vec<(String, Vec<AdminEvent<T>>)>
}

impl<T> Serialize for AdminEventResponse<T>
    where T: Serialize {
    fn serialize<S>(&self, serializer: &mut S) -> Result<(), S::Error>
        where S: Serializer
    {
        let mut state = serializer.serialize_map(Some(self.pairs.len()))?;
        for &(ref k, ref v) in &self.pairs {
            serializer.serialize_map_key(&mut state, k)?;
            serializer.serialize_map_value(&mut state, v)?;
        }
        serializer.serialize_map_end(state)
    }
}


#[derive(Serialize)]
pub struct AdminEvent<T>
    where T: Serialize {
    date: String,
    value: f64,
    context: T
}

#[derive(Serialize)]
pub struct Container<'a, T: 'a>
    where T: Serialize {
    pub _type: String,
    pub data: &'a T
}

impl<'a, T> Container<'a, T>
where T: Serialize {
    pub fn new_err(error_message: &'a T) -> Container<'a, T> {
        return Container {
            _type: "ERROR".to_owned().to_owned(),
            data: error_message
        }
    }
}